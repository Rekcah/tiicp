TIICP-cli 1.0.0  Copyright (C) 2021  Activating Evolution
This program comes with ABSOLUTELY NO WARRANTY; for details type 'show w'.
This is free software, and you are welcome to redistribute it
under certain conditions; type `show c' for details.

![](https://gitlab.com/Rekcah/tiicp/-/raw/main/templates/logo.png)

Versione: 1.1.0

Installazione:./install.sh

Uso:
python3.9 tiicp-cli.py

Dipendenze:
	Pacchetti:
		aria2c/aria2-fast
		python3.9
		ipfs
		libreoffice
		bzip2
		tar
		xz
		pip3
	Librerie python:
		pick
		numpy
		time
		pycurl
		re
		random
		
La scelta di utilizzare l'italiano per questo programma è dato unicamente dalla scelta di promuovere la lingua italiana e favorirne un suo sviluppo a discapito di quella inglese,
ugualmente sostenuta ma in minore forma, dal momento che l'ontologia di Activating Evolution è stata sviluppata in forma primaria in lingua italiana.
Pertanto qualsiasi traduzione potrà essere svolta manualmente, in quanto il software è provvisto di licenza GNU GPL v.3 (leggere il file LICENSE per sapere cosa è possibile fare con questo programma).
Il programma è ad utilizzo ristretto ed esclusivo dei membri affiliati al progetto di Activating Evolution e al progetto The Instant Indipendence Crafting Project.
I motivi per cui è stata scelta la rete ipfs anziché altre rete sono in prima istanza che ipfs è peer to peer, dunque è una rete decentralizzata sulla quale chiunque può condividere i propri file o riceverli, senza fare capo ad un server centralizzato.
Il secondo motivo è che la rete garantisce l'anonimato e soprattutto l'assenza di censura. Infatti un file caricato sulla rete ipfs difficilmente potrà essere rimosso, in quanto il file per essere eliminato dovrebbe essere eliminato da tutti i nodi della rete che lo dispongono.
Pertanto vi richiediamo di prestare cautela ai file caricati, in quanto potranno essere visibili sensibilmente a chiunque abbia a disposizione il link al file collegato.
I file potranno essere visibili anche dall'esterno consultando il link https://ipfs.io/ipfs/<HASH> dove al posto di <HASH> potrà essere immesso l'hash creato al momento del caricamento del file.
Per una più facile gestione dei file consigliamo, una volta avviata la rete ipfs, di condividere i file tramite interfaccia grafica da browser all'indirizzo indicato al momento dell'installazione di ipfs.

![](https://gitlab.com/Rekcah/tiicp/-/raw/main/templates/TIICP.gif "TIICP logo")

Per segnalare un bug o altro: info@activatingevolution.com