#!/bin/bash

name=$(cat /etc/*release | grep -i "name" | head -n1 | sed 's/NAME=//g' | sed 's/"//g')   # nome della distro
directory_installazione=$(pwd)

defaultPythonVersion="python3.9"
echo "Avvio l'installazione del programma The Instant Indipendent Crafting Project."

#Funzione di installazione delle librerie python
verifica_librerie_python () {
  #librerie python
  echo "Pacchetti installati con successo, procedo ad installare le librerie python."

  #pick
  echo "Verificando la presenza della libreria pick..."
  if [ $($defaultPythonVersion -c "import pick"; echo $?) -eq 0 ]
  then
    echo "pick [OK]"
  else
    echo "Installazione della libreria pick in corso..."
    pip3 install pick
    if [ $? -eq 0 ]
    then
      echo "Libreria pick installata correttamente."
    else
      echo "Installare la libreria pick manualmente."
      exit
    fi
  fi

  #numpy
  echo "Verificando la presenza della libreria numpy..."
  if [ $($defaultPythonVersion -c "import numpy"; echo $?) -eq 0 ]
  then
    echo "numpy [OK]"
  else
    echo "Installazione della libreria numpy in corso..."
    pip3 install numpy
    if [ $? -eq 0 ]
    then
      echo "Libreria numpy installata correttamente."
    else
      echo "Installare la libreria numpy manualmente."
      exit
    fi
  fi

  #time
  echo "Verificando la presenza della libreria time..."
  if [ $($defaultPythonVersion -c "import time"; echo $?) -eq 0 ]
  then
    echo "time [OK]"
  else
    echo "Installazione della libreria time in corso..."
    pip3 install time
    if [ $? -eq 0 ]
    then
      echo "Libreria time installata correttamente."
    else
      echo "Installare la libreria time manualmente."
      exit
    fi
  fi

  #pycurl
  echo "Verificando la presenza della libreria pycurl..."
  if [ $($defaultPythonVersion -c "import pycurl"; echo $?) -eq 0 ]
  then
    echo "pycurl [OK]"
  else
    echo "Installazione della libreria pycurl in corso..."
    pip3 install pycurl
    if [ $? -eq 0 ]
    then
      echo "Libreria pycurl installata correttamente."
    else
      echo "Installare la libreria pycurl manualmente."
      exit
    fi
  fi

  #re
  echo "Verificando la presenza della libreria re..."
  if [ $($defaultPythonVersion -c "import re"; echo $?) -eq 0 ]
  then
    echo "re [OK]"
  else
    echo "Installazione della libreria re in corso..."
    pip3 install re
    if [ $? -eq 0 ]
    then
      echo "Libreria re installata correttamente."
    else
      echo "Installare la libreria re manualmente."
      exit
    fi
  fi

  #os
  echo "Verificando la presenza della libreria os..."
  if [ $($defaultPythonVersion -c "import os"; echo $?) -eq 0 ]
  then
    echo "os [OK]"
  else
    echo "Installazione della libreria os in corso..."
    pip3 install os
    if [ $? -eq 0 ]
    then
      echo "Libreria os installata correttamente."
    else
      echo "Installare la libreria os manualmente."
      exit
    fi
  fi

  #random
  echo "Verificando la presenza della libreria random..."
  if [ $($defaultPythonVersion -c "import random"; echo $?) -eq 0 ]
  then
    echo "random [OK]"
  else
    echo "Installazione della libreria random in corso..."
    pip3 install random
    if [ $? -eq 0 ]
    then
      echo "Libreria random installata correttamente."
    else
      echo "Installare la libreria random manualmente."
      exit
    fi
  fi
}

#Funzione per la creazione delle directory
crea_directory() {
  echo "Inserisci il nome della cartella dei pdf ricordandoti di mettere lo slash alla fine (es.: /home/user/Documents/pdf/):"
  read -ep "> " cartella_pdf
  if [ ! -d $cartella_pdf ]
  then
    echo "Il path inserito non risulta presente, riprovare con un path alternativo."
    echo "Esco..."
    exit
  fi
  echo $cartella_pdf | sed 's/\n//g' > repository/pdf.conf
  echo "Inserisci il nome della cartella dei file diy ricordandoti di mettere lo slash alla fine (es.: /home/user/Documents/diy/):"
  read -ep "> " cartella_diy
  if [ ! -d $cartella_pdf ]
  then
    echo "Il path inserito non risulta presente, riprovare con un path alternativo."
    echo "Esco..."
    exit
  fi
  echo $cartella_diy | sed 's/\n//g' > repository/diy.conf
  echo
  echo "Installazione effettuata correttamente."
}


#Check del sistema operativo
#Debian like
if [ $(echo $name | grep -qi debian; echo $?) -eq 0 ]
then
  echo "Rilevato sistema Debian."
  echo "Preparazione all'installazione delle dipendenze..."

  #$defaultPythonVersion
  echo "Verificando la presenza di $defaultPythonVersion nel sistema..."
  if ! command -v $defaultPythonVersion &> /dev/null
  then
    echo "$defaultPythonVersion non trovato, procedo ad installarlo."
    if [ $(grep "deb http://ftp.de.debian.org/debian testing main" /etc/apt/sources.list; echo $?) -eq 1 ]
    then
      sudo echo "deb http://ftp.de.debian.org/debian testing main" >> /etc/apt/sources.list
    fi
    echo 'APT::Default-Release "stable";' | sudo tee -a /etc/apt/apt.conf.d/00local
    sudo apt-get update
    sudo apt-get -t testing install $defaultPythonVersion
    if [ $? -eq 0]
    then
      echo "$defaultPythonVersion installato con successo!"
      if [[ $(echo $defaultPythonVersion | sed 's/^.*Python//g' | sed 's/\ //g') != $(python --version | sed 's/^.*\ //g' | grep -Po '^[^\.]*\.[^\.]*|.*$' | head -n1) ]]
      then
        echo -e "\e[1;33mAttenzione, stai usando la versione $(python --version) di python!"
      fi
    else
      echo "Non sono riuscito ad installare $defaultPythonVersion, prova ad installarlo manualmente."
      exit
    fi
  else
    echo "$defaultPythonVersion [OK]"
    echo
  fi

  #pip3
  echo "Verificando la presenza di pip3 nel sistema..."
  if ! command -v pip3 &> /dev/null
  then
    echo "pip3 non trovato, procedo ad installarlo."
    sudo apt install python3-venv python3-pip
    if [ $? -eq 0 ]
    then
      echo "pip3 installato con successo!"
    else
      echo "Non sono riuscito ad installare pip3, prova ad installarlo manualmente."
      exit
    fi
  else
    echo "pip3 [OK]"
    echo
  fi

  #aria2c
  echo "Verificando la presenza di aria2c nel sistema..."
  if ! command -v aria2c &> /dev/null
  then
    echo "aria2c non trovato, procedo ad installarlo."
    echo "Installazione di snapd e di core in corso al fine di installare aria2c..."
    sudo apt update
    sudo apt install snapd
    sudo snap install core
    sudo snap install aria2c
    if [ $? -eq 0 ]
    then
      echo "aria2c installato con successo!"
    else
      echo "Non sono riuscito ad installare aria2c, prova ad installarlo manualmente."
      exit
    fi
  else
    echo "aria2c [OK]"
    echo
  fi

  #libreoffice
  echo "Verificando la presenza di libreoffice nel sistema..."
  if ! command -v libreoffice &> /dev/null
  then
    echo "libreoffice non trovato, procedo ad installarlo."
    sudo apt-get install libreoffice
    if [ $? -eq 0 ]
    then
      echo "Libreoffice installato con successo!"
    else
      echo "Non sono riuscito ad installare libreoffice, prova ad installarlo manualmente."
      exit
    fi
  else
    echo "libreoffice [OK]"
    echo
  fi

  #bzip2
  echo "Verificando la presenza di bzip2 nel sistema...."
  if ! command -v bzip2 &> /dev/null
  then
    echo "bzip2 non trovato, procedo ad installarlo."
    sudo apt-get install bzip2
    if [ $? -eq 0 ]
    then
      echo "bzip2 installato con successo!"
    else
      echo "Non sono riuscito ad installare bzip2, prova ad installarlo manualmente."
      exit
    fi
  else
    echo "bzip2 [OK]"
    echo
  fi

  #tar
  echo "Verificando la presenza di tar nel sistema...."
  if ! command -v tar &> /dev/null
  then
    echo "tar non trovato, procedo ad installarlo."
    sudo apt-get install tar
    if [ $? -eq 0 ]
    then
      echo "tar installato con successo!"
    else
      echo "Non sono riuscito ad installare tar, prova ad installarlo manualmente."
      exit
    fi
  else
    echo "tar [OK]"
    echo
  fi

  #xz
  echo "Verificando la presenza di xz nel sistema...."
  if ! command -v xz &> /dev/null
  then
    echo "xz non trovato, procedo ad installarlo."
    sudo apt-get install xz
    if [ $? -eq 0 ]
    then
      echo "xz installato con successo!"
    else
      echo "Non sono riuscito ad installare xz, prova ad installarlo manualmente."
      exit
    fi
  else
    echo "xz [OK]"
    echo
  fi

  #ipfs
  echo "Verificando la presenza di ipfs nel sistema..."
  if ! command -v ipfs &> /dev/null
  then
    echo "ipfs non trovato, procedo ad installarlo."
    sudo snap install ipfs-desktop
    if [ $? -eq 0 ]
    then
      echo "ipfs installato con successo!"
    else
      echo "Non sono riuscito ad installare ipfs, prova ad installarlo manualmente."
      exit
    fi
  else
    echo "ipfs [OK]"
    echo
  fi
  verifica_librerie_python
  crea_directory

#Arch Linux like
elif [ $(echo $name | grep -qi arch; echo $?) -eq 0 ]
then
  echo "Rilevato sistema Arch Linux."
  echo "Preparazione all'installazione delle dipendenze..."

  #Python
  echo "Verificando la presenza di $defaultPythonVersion nel sistema..."
  if ! command -v $defaultPythonVersion &> /dev/null
  then
    echo "$defaultPythonVersion non trovato, procedo ad installarlo."
    sudo pacman -S python
  else
    echo "$defaultPythonVersion [OK]"
    echo
  fi

  #pip3
  echo "Verificando la presenza di pip3 nel sistema..."
  if ! command -v pip3 &> /dev/null
  then
    echo "pip3 non trovato, procedo ad installarlo."
    sudo pacman python-pip
    if [ $? -eq 0 ]
    then
      echo "pip3 installato con successo!"
    else
      echo "Non sono riuscito ad installare pip3, prova ad installarlo manualmente."
      exit
    fi
  else
    echo "pip3 [OK]"
    echo
  fi

  #aria2c
  echo "Verificando la presenza di aria2c nel sistema..."
  if ! command -v aria2c &> /dev/null
  then
    echo "aria2c non trovato, procedo ad installarlo."
    echo "Verifico la presenza di git..."
    if ! command -v git &> /dev/null
    then
      sudo pacman -S git
      if [ $? -eq 1 ]
      then
        echo "Installare git manualmente."
        exit
      fi
    fi
    cd /tmp
    git clone https://aur.archlinux.org/aria2-fast.git
    cd aria2-fast
    makepkg -si
    if [ $? -eq 0 ]
    then
      echo "aria2c installato con successo!"
    else
      echo "Non sono riuscito ad installare aria2c (aria2-fast), prova ad installarlo manualmente."
      exit
    fi
  else
    echo "aria2c [OK]"
    echo
  fi

  #libreoffice
  echo "Verificando la presenza di libreoffice nel sistema..."
  if ! command -v libreoffice &> /dev/null
  then
    echo "libreoffice non trovato, procedo ad installarlo."
    sudo pacman -S libreoffice
    if [ $? -eq 0 ]
    then
      echo "Libreoffice installato con successo!"
    else
      echo "Non sono riuscito ad installare libreoffice, prova ad installarlo manualmente."
      exit
    fi
  else
    echo "libreoffice [OK]"
    echo
  fi

  #bzip2
  echo "Verificando la presenza di bzip2 nel sistema...."
  if ! command -v bzip2 &> /dev/null
  then
    echo "bzip2 non trovato, procedo ad installarlo."
    sudo pacman -S bzip2
    if [ $? -eq 0 ]
    then
      echo "bzip2 installato con successo!"
    else
      echo "Non sono riuscito ad installare bzip2, prova ad installarlo manualmente."
      exit
    fi
  else
    echo "bzip2 [OK]"
    echo
  fi

  #tar
  echo "Verificando la presenza di tar nel sistema...."
  if ! command -v tar &> /dev/null
  then
    echo "tar non trovato, procedo ad installarlo."
    sudo pacman -S tar
    if [ $? -eq 0 ]
    then
      echo "tar installato con successo!"
    else
      echo "Non sono riuscito ad installare tar, prova ad installarlo manualmente."
      exit
    fi
  else
    echo "tar [OK]"
    echo
  fi

  #xz
  echo "Verificando la presenza di xz nel sistema...."
  if ! command -v xz &> /dev/null
  then
    echo "xz non trovato, procedo ad installarlo."
    sudo pacman -S xz
    if [ $? -eq 0 ]
    then
      echo "xz installato con successo!"
    else
      echo "Non sono riuscito ad installare xz, prova ad installarlo manualmente."
      exit
    fi
  else
    echo "xz [OK]"
    echo
  fi

  #ipfs
  echo "Verificando la presenza di ipfs nel sistema..."
  if ! command -v ipfs &> /dev/null
  then
    echo "ipfs non trovato, procedo ad installarlo."
    sudo pacman -S go-ipfs
    if [ $? -eq 0 ]
    then
      echo "ipfs installato con successo!"
      echo "Congiruazione di ipfs in corso..."
      ipfs init
      ipfs daemon
      systemctl --user enable --now ipfs.service
      if [ $? -eq 1]
      then
        echo "Ci sono stati degli errori nella configurazione automatica di ipfs, configurarlo manualmente."
        exit
      fi
    else
      echo "Non sono riuscito ad installare ipfs, prova ad installarlo manualmente."
      exit
    fi
  else
    echo "ipfs [OK]"
    echo
  fi

  verifica_librerie_python
  crea_directory

#Ubuntu like
elif [ $(echo $name | grep -qi ubuntu; echo $?) -eq 0 ]
then
  echo "Rilevato sistema Ubuntu."
  echo "Preparazione all'installazione delle dipendenze..."

  #$defaultPythonVersion
  echo "Verificando la presenza di $defaultPythonVersion nel sistema..."
  if ! command -v $defaultPythonVersion &> /dev/null
  then
    echo "$defaultPythonVersion non trovato, procedo ad installarlo."
    sudo add-apt-repository ppa:deadsnakes/ppa
    sudo apt-get update
    sudo apt install $defaultPythonVersion
    if [ $? -eq 0]
    then
      echo "$defaultPythonVersion installato con successo!"
    else
      echo "Non sono riuscito ad installare $defaultPythonVersion, prova ad installarlo manualmente."
      exit
    fi
  else
    echo "$defaultPythonVersion [OK]"
    echo
  fi

  #pip3
  echo "Verificando la presenza di pip3 nel sistema..."
  if ! command -v pip3 &> /dev/null
  then
    echo "pip3 non trovato, procedo ad installarlo."
    sudo apt install python3-venv python3-pip
    if [ $? -eq 0 ]
    then
      echo "pip3 installato con successo!"
    else
      echo "Non sono riuscito ad installare pip3, prova ad installarlo manualmente."
      exit
    fi
  else
    echo "pip3 [OK]"
    echo
  fi

  #aria2c
  echo "Verificando la presenza di aria2c nel sistema..."
  if ! command -v aria2c &> /dev/null
  then
    echo "aria2c non trovato, procedo ad installarlo."
    echo "Installazione di snapd e di core in corso al fine di installare aria2c..."
    sudo apt update
    sudo apt install snapd
    sudo snap install core
    sudo snap install aria2c
    if [ $? -eq 0 ]
    then
      echo "aria2c installato con successo!"
    else
      echo "Non sono riuscito ad installare aria2c, prova ad installarlo manualmente."
      exit
    fi
  else
    echo "aria2c [OK]"
    echo
  fi

  #libreoffice
  echo "Verificando la presenza di libreoffice nel sistema..."
  if ! command -v libreoffice &> /dev/null
  then
    echo "libreoffice non trovato, procedo ad installarlo."
    sudo apt-get install libreoffice
    if [ $? -eq 0 ]
    then
      echo "Libreoffice installato con successo!"
    else
      echo "Non sono riuscito ad installare libreoffice, prova ad installarlo manualmente."
      exit
    fi
  else
    echo "libreoffice [OK]"
    echo
  fi

  #bzip2
  echo "Verificando la presenza di bzip2 nel sistema...."
  if ! command -v bzip2 &> /dev/null
  then
    echo "bzip2 non trovato, procedo ad installarlo."
    sudo apt-get install bzip2
    if [ $? -eq 0 ]
    then
      echo "bzip2 installato con successo!"
    else
      echo "Non sono riuscito ad installare bzip2, prova ad installarlo manualmente."
      exit
    fi
  else
    echo "bzip2 [OK]"
    echo
  fi

  #tar
  echo "Verificando la presenza di tar nel sistema...."
  if ! command -v tar &> /dev/null
  then
    echo "tar non trovato, procedo ad installarlo."
    sudo apt-get install tar
    if [ $? -eq 0 ]
    then
      echo "tar installato con successo!"
    else
      echo "Non sono riuscito ad installare tar, prova ad installarlo manualmente."
      exit
    fi
  else
    echo "tar [OK]"
    echo
  fi

  #xz
  echo "Verificando la presenza di xz nel sistema...."
  if ! command -v xz &> /dev/null
  then
    echo "xz non trovato, procedo ad installarlo."
    sudo apt-get install xz
    if [ $? -eq 0 ]
    then
      echo "xz installato con successo!"
    else
      echo "Non sono riuscito ad installare xz, prova ad installarlo manualmente."
      exit
    fi
  else
    echo "xz [OK]"
    echo
  fi

  #ipfs
  echo "Verificando la presenza di ipfs nel sistema..."
  if ! command -v ipfs &> /dev/null
  then
    echo "ipfs non trovato, procedo ad installarlo."
    sudo snap install ipfs-desktop
    if [ $? -eq 0 ]
    then
      echo "ipfs installato con successo!"
    else
      echo "Non sono riuscito ad installare ipfs, prova ad installarlo manualmente."
      exit
    fi
  else
    echo "ipfs [OK]"
    echo
  fi
  verifica_librerie_python
  crea_directory

#Fedora like
elif [ $(echo $name | grep -qi fedora; echo $?) -eq 0 ]
then
  echo "Rilevato sistema Fedora."
  echo "Preparazione all'installazione delle dipendenze..."

  #$defaultPythonVersion
  echo "Verificando la presenza di $defaultPythonVersion nel sistema..."
  if ! command -v $defaultPythonVersion &> /dev/null
  then
    echo "$defaultPythonVersion non trovato, procedo ad installarlo."
    sudo dnf install python36
    if [ $? -eq 0]
    then
      echo "$defaultPythonVersion installato con successo!"
    else
      echo "Non sono riuscito ad installare $defaultPythonVersion, prova ad installarlo manualmente."
      exit
    fi
  else
    echo "$defaultPythonVersion [OK]"
    echo
  fi

  #pip3
  echo "Verificando la presenza di pip3 nel sistema..."
  if ! command -v pip3 &> /dev/null
  then
    echo "pip3 non trovato, procedo ad installarlo."
    sudo dnf install python3-pip
    if [ $? -eq 0 ]
    then
      echo "pip3 installato con successo!"
    else
      echo "Non sono riuscito ad installare pip3, prova ad installarlo manualmente."
      exit
    fi
  else
    echo "pip3 [OK]"
    echo
  fi

  #aria2c
  echo "Verificando la presenza di aria2c nel sistema..."
  if ! command -v aria2c &> /dev/null
  then
    echo "aria2c non trovato, procedo ad installarlo."
    echo "Installazione di snapd e di core in corso al fine di installare aria2c..."
    sudo dnf install snapd
    sudo ln -s /var/lib/snapd/snap /snap
    sudo snap install aria2c
    if [ $? -eq 0 ]
    then
      echo "aria2c installato con successo!"
    else
      echo "Non sono riuscito ad installare aria2c, prova ad installarlo manualmente."
      exit
    fi
  else
    echo "aria2c [OK]"
    echo
  fi

  #libreoffice
  echo "Verificando la presenza di libreoffice nel sistema..."
  if ! command -v libreoffice &> /dev/null
  then
    echo "libreoffice non trovato, procedo ad installarlo."
    sudo dnf install snapd
    sudo ln -s /var/lib/snapd/snap /snap
    sudo snap install libreoffice
    if [ $? -eq 0 ]
    then
      echo "Libreoffice installato con successo!"
    else
      echo "Non sono riuscito ad installare libreoffice, prova ad installarlo manualmente."
      exit
    fi
  else
    echo "libreoffice [OK]"
    echo
  fi

  #bzip2
  echo "Verificando la presenza di bzip2 nel sistema...."
  if ! command -v bzip2 &> /dev/null
  then
    echo "bzip2 non trovato, procedo ad installarlo."
    sudo dnf install bzip2
    if [ $? -eq 0 ]
    then
      echo "bzip2 installato con successo!"
    else
      echo "Non sono riuscito ad installare bzip2, prova ad installarlo manualmente."
      exit
    fi
  else
    echo "bzip2 [OK]"
    echo
  fi

  #tar
  echo "Verificando la presenza di tar nel sistema...."
  if ! command -v tar &> /dev/null
  then
    echo "tar non trovato, procedo ad installarlo."
    sudo dnf install tar
    if [ $? -eq 0 ]
    then
      echo "tar installato con successo!"
    else
      echo "Non sono riuscito ad installare tar, prova ad installarlo manualmente."
      exit
    fi
  else
    echo "tar [OK]"
    echo
  fi

  #xz
  echo "Verificando la presenza di xz nel sistema...."
  if ! command -v xz &> /dev/null
  then
    echo "xz non trovato, procedo ad installarlo."
    sudo dnf install xz
    if [ $? -eq 0 ]
    then
      echo "xz installato con successo!"
    else
      echo "Non sono riuscito ad installare xz, prova ad installarlo manualmente."
      exit
    fi
  else
    echo "xz [OK]"
    echo
  fi

  #ipfs
  echo "Verificando la presenza di ipfs nel sistema..."
  if ! command -v ipfs &> /dev/null
  then
    echo "ipfs non trovato, procedo ad installarlo."
    sudo dnf install snapd
    sudo ln -s /var/lib/snapd/snap /snap
    sudo snap install ipfs
    if [ $? -eq 0 ]
    then
      echo "ipfs installato con successo!"
    else
      echo "Non sono riuscito ad installare ipfs, prova ad installarlo manualmente."
      exit
    fi
  else
    echo "ipfs [OK]"
    echo
  fi
  verifica_librerie_python
  crea_directory

else
  echo  "Non ho rilevato alcun sistema operativo conosciuto, installare manualmente i pacchetti:
  $defaultPythonVersion
  libreoffice
  tar
  xz
  bzip2
  ipfs
  Ed installare le seguenti librerie python:
  pick
  numpy
  time
  pycurl
  re
  os
  random"

fi

echo "Digitare python tiicp-cli.py per eseguire il programma."