#!/usr/bin/python3.6
# -*- coding: utf-8 -*-
import logging
import time
import os
from pick import pick
import requests
import random

#Variabili modificabili
nome_programma_tiicp = '''
 __ __| _ _| _ _|   ___|   _ \  
    |     |    |   |      |   | 
    |     |    |   |      ___/  
   _|   ___| ___| \____| _|     
        v. 1.1.0
  - Come and take it! -\n'''                                                                              # nome del programma

with open('repository/frasi.local') as frasi_file:
    frasi = frasi_file.readlines()
frasi = [frase.strip() for frase in frasi]
frasi = list(filter(None, frasi))
tot_frasi = len(frasi)-1
nfrase = random.randint(0,tot_frasi)
frase_del_momento = frasi[nfrase].replace("\\n","\n")
nome_programma = nome_programma_tiicp+"|#> "+frase_del_momento+" <#|"

indietro = "<< Indietro <<"                                                                             # voce che specifica il tasto indietro

#Legge file di configurazione path pdf e diy
fdiy = open("repository/diy.conf", "r")
fpdf = open("repository/pdf.conf", "r")
working_directory_pdf = (fpdf.read()).replace("\n", "")                                                 # cartella dei file pdf
working_directory_diy = (fdiy.read()).replace("\n", "")                                                 # cartella dei file diy
if os.path.exists(working_directory_pdf) and os.path.exists(working_directory_diy):
    pass
else:
    print("Eseguire il file install.sh per avviare l'installazione.")
    exit(1)

repository = "repository/"         # cartella e file dei repository
numero_file_diy = len([name for name in os.listdir(working_directory_diy) if os.path.isfile(os.path.join(working_directory_diy, name))])    # numero dei file diy presenti
# Variabili necessarie
titolo = []
path = []
lista_elementi = []
pathdiy = []

#Animazione di start
def animation():
    distanceFromTop = 10
    n = 0
    while n < 10:
        print("\n" * distanceFromTop)
        print('''
    ------------ --------  --------  ------------ -----------  
    ************ ********  ********  ************ ************ 
    ------------   ----      ----    ---          ---      --- 
        ****       ****      ****    ***          ************ 
        ----       ----      ----    ---          -----------  
        ****       ****      ****    ***          ****         
        ----     --------  --------  ------------ ----         
        ****     ********  ********  ************ ****         
                                                                    
    ___                                                        __      _            __             
     | |_  _  | _  _|_ _  _ |_  | _  _|. _  _ _  _| _ _  _ _  /   _ _ (_|_. _  _   |__)_ _ . _ _|_ 
     | | )(-  || )_)|_(_|| )|_  || )(_|||_)(-| )(_|(-| )(_(-  \__| (_|| |_|| )(_)  |  | (_)|(-(_|_ 
                                        |                                     _/           /                  
      ''')
        time.sleep(0.2)                                                                         #velocita' banner
        os.system('clear')
        distanceFromTop -= 1
        n += 1
        if distanceFromTop < 0:
            distanceFromTop = 10


#Banner
def banner():                                                                                  # banner per gli input dato che non hanno la funzione data da pick
    os.system('clear')
    print ("")
    print (" "+nome_programma)
    print ("")

def leggi_diy():
    # Apre i file diy e ne ricava delle variabili
    lines = []
    titolo.clear()
    path.clear()
    pathdiy.clear()
    for diyfile in os.listdir(working_directory_diy):                                         # per ogni file diy nel path indicato
        if diyfile.endswith(".diy") or diyfile.endswith(".DIY"):                              # se il file finisce con diy o con DIY
            diyaperto = working_directory_diy + diyfile
            with open(diyaperto, "r") as diyletto:                                            # allora leggi il file
                lines = diyletto.readlines()                                                  # linee di diy lette e messe in una lista
                lines = ([s.strip('\n') for s in lines])                                      # rimuove gli a capo
                pathdiy.append(diyaperto)

                titolo.append(lines[1])                                                       # Lista dei titoli
                path.append(lines[-1])                                                        # Lista dei path dei diy

                #Leggi file diy
                if "[elementi]" in lines:
                    indice_elementi = lines.index("[elementi]") + 1                           # posizione dell'indice [elementi]
                    indice_acapo = lines.index("[pdf]") - 5                                   # posizione a capo prima di [pdf]
                    del lines[0:indice_elementi]                                              # elimina prima di elementi
                    del lines[indice_acapo:]                                                  # elimina dopo gli elementi
                    lista_elementi.append(lines)                                              # Lista degli elementi
        else:
            lines.clear()
            pathdiy.clear()
            titolo.clear()
            path.clear()


#Funzione della schermata iniziale
def start():
    options = ['Cosa posso costruire con...', 'Come posso costruire...', 'Cerca manualmente tra le guide...', 'Crea pdf...', 'Crea nuova guida...', 'Carica guida online...', 'Cerca guida online...', 'Elimina guide...', 'Esci.']
    options, index = pick(options, nome_programma, ">")
    if index == 0:
        banner()
        CosaPossoCostruireCon()
    elif index == 1:
        ComePossoCostruire()
    elif index == 2:
        CercaManualmenteTraLeGuide()
    elif index == 3:
        CreaPDF()
    elif index == 4:
        CreaGuida()
    elif index == 5:
        CaricaOnline()
    elif index == 6:
        CercaOnline()
    elif index == 7:
        cancellaGuide()
    elif index == 8:
        os.system("clear")
        exit()
    else:
        os.system("clear")
        print ("Non so come tu abbia fatto a selezionare questa voce ma non hai selezionato nulla.")
        exit()


# Cosa posso costruire con...
# Ossia cosa puoi costruire avendo a disposizione alcuni strumenti
# Ovviamente la lista degli strumenti che si va ad inserire fanno riferimento ad un solo progetto, non elementi separati per ciascun progetto
def CosaPossoCostruireCon():
    leggi_diy()
    global item_elemento_da_ricercare
    pdf_trovati = []
    path_pdf_trovati = []
    elementi_progetto = []
    # Variabili necessarie

    # Input degli elementi a disposizione
    print ("  Cosa posso costruire con...")
    print ("  Inserisci gli elementi separandoli da una virgola ed uno spazio:")
    elementi = input("  > ")                                                                  # input elementi da ricercare
    elementi_da_ricercare = elementi.split(", ")                                              # va a riconoscere gli elementi separandoli da uno spazio

    if not elementi:                                                                          # se il campo di ricerca è vuoto allora ripete il ciclo
        os.system("clear")
        print ("Errore, hai lasciato vuoto il campo di ricerca, riprova. ")
        os.system("sleep 1 && clear")
        banner()
        CosaPossoCostruireCon()

    if not lista_elementi:
        os.system("clear")
        print(nome_programma,"\n \n Non ho trovato nessuna guida, torno alla home.")
        os.system("sleep 1.5")
        start()

    for item_elemento_da_ricercare in elementi_da_ricercare:
        item_elemento_da_ricercare = [item_elemento_da_ricercare.lower()]                     # mette in minuscolo gli elementi da ricercare
    for item_lista_elementi in range(numero_file_diy):
        check_elementi = all(item in [x.lower() for x in lista_elementi[item_lista_elementi]] for item in item_elemento_da_ricercare)   # verifica il campo di ricerca corrisponde al contenuto dei file in minuscolo
        if check_elementi is True:
            pdf_trovati.append(titolo[item_lista_elementi])                                   # titoli dei pdf trovati
            path_pdf_trovati.append(path[item_lista_elementi])                                # path dei pdf trovati
            elementi_progetto.append(lista_elementi[item_lista_elementi])

    if len(elementi_progetto) == 0:                                                           # se non trova nulla
        title = (nome_programma + "\n \nNon ho trovato alcuna guida in merito, scegli l\'azione da intraprendere:")
        scelta_diy_non_trovato = ["Cerca online", "Crea una guida", "Torna al menu principale."]
        scelta_diy_non_trovato, index_diy_non_trovato = pick(scelta_diy_non_trovato, title, ">")
        if index_diy_non_trovato == 0:  # cerca come produrlo, su internet
            title = (nome_programma + "\n \nScegli cosa vuoi cercare su internet:")
            options = elementi_da_ricercare
            options, index = pick(options, title, ">")
            os.system("clear")
            dork = elementi_da_ricercare[index].replace(" ", "+")
            os.system("xdg-open https://duckduckgo.com/?q=" + dork + "+produzione&t=ffab&atb=v258-4__&ia=web")
            os.system("xdg-open https://duckduckgo.com/?q=" + dork + "+diy&t=ffab&atb=v258-4__&ia=web")
            os.system("xdg-open https://duckduckgo.com/?q=" + dork + "+fai+da+te&t=ffab&atb=v258-4__&ia=web")
            os.system("xdg-open https://duckduckgo.com/?q=come+produrre+" + dork + "&t=ffab&atb=v258-4__&ia=web")
            os.system("xdg-open https://duckduckgo.com/?q=" + dork + "+diy&t=ffab&atb=v258-4__&ia=web")
            os.system("xdg-open https://it.wikipedia.org/wiki/" + dork)
            os.system("sleep 0.5 && clear")
            pdf_trovati.clear()
            start()
        elif index_diy_non_trovato == 1:
            # CreaGuida()
            pdf_trovati.clear()
            CreaGuida()
        else:
            print("Torno al menu principale...")
            os.system("sleep 0.5 && clear")
            pdf_trovati.clear()
            start()

    title = (nome_programma + "\n \nHo trovato i seguenti progetti:")                         # fa selezionare uno dei progetti trovati
    pdf_scelto = pdf_trovati
    if indietro not in pdf_scelto:
        pdf_scelto.append(indietro)
    pdf_scelto, index_scelta = pick(pdf_scelto, title, ">")
    if pdf_scelto == indietro:
        start()

    title = (nome_programma + "\n \nGli elementi presenti nella lista sono: \n" + str(elementi_progetto[index_scelta]) + "\nDisponi di tutti gli elementi della lista?")  # hai tutti gli elementi della lista?
    opzione_scelta = ["Si", "No"]
    opzione_scelta, index_ele = pick(opzione_scelta, title, ">")
    if index_ele == 0:  # ci sono tutti gli elementi della lista
        os.system("clear")
        os.system("xdg-open " + path_pdf_trovati[index_scelta])
        os.system("sleep 0.5 && clear")
        pdf_trovati.clear()
        start()
    else:  # mancano degli elementi
        title = (nome_programma + "\n \nScegli gli elementi che ti mancano:")
        options = elementi_progetto[index_scelta]
        options, index_elem_scelto = pick(options, title, ">")
        # Check se è presente qualche diy
        if options in [item.lower() for item in titolo]:                                      # verifica se l'elemento selezionato si trova nella lista dei titoli
            os.system("clear")
            os.system("xdg-open " + path[[item.lower() for item in titolo].index(options)])   # apre il pdf dell'elemento cercato
            os.system("sleep 0.5 && clear")
            pdf_trovati.clear()
            start()
        else:
            title = (nome_programma + "\n \nNon ho trovato alcuna guida in merito, scegli l\'azione da intraprendere:")
            scelta_diy_non_trovato = ["Cerca online", "Crea una guida", "Torna al menu principale."]
            scelta_diy_non_trovato, index_diy_non_trovato = pick(scelta_diy_non_trovato, title, ">")
            if index_diy_non_trovato == 0:                                                    #cerca come produrlo, su internet
                os.system("clear")
                dork = options.replace(" ", "+")
                os.system("xdg-open https://duckduckgo.com/?q=" + dork + "+produzione&t=ffab&atb=v258-4__&ia=web")
                os.system("xdg-open https://duckduckgo.com/?q=" + dork + "+diy&t=ffab&atb=v258-4__&ia=web")
                os.system("xdg-open https://duckduckgo.com/?q=" + dork + "+fai+da+te&t=ffab&atb=v258-4__&ia=web")
                os.system("xdg-open https://duckduckgo.com/?q=come+produrre+" + dork + "&t=ffab&atb=v258-4__&ia=web")
                os.system("xdg-open https://duckduckgo.com/?q=" + dork + "+diy&t=ffab&atb=v258-4__&ia=web")
                os.system("xdg-open https://it.wikipedia.org/wiki/" + dork)
                os.system("sleep 0.5 && clear")
                pdf_trovati.clear()
                start()
            elif index_diy_non_trovato == 1:
                #CreaGuida()
                pdf_trovati.clear()
                CreaGuida()
            else:
                print("Torno al menu principale...")
                os.system("sleep 0.5 && clear")
                pdf_trovati.clear()
                start()



def ComePossoCostruire():
    leggi_diy()
    progetti_trovati = []
    #Input degli elementi a disposizione
    banner()
    print ("  Come posso costruire...")
    print ("  Inserisci il nome dello strumento che tu vuoi costruire:")
    progetto = input("  > ")
    progetto = str(progetto.lower())

    if not progetto:                                                                          # se il campo di ricerca è vuoto allora ripete il ciclo
        os.system("clear")
        print ("Errore, hai lasciato vuoto il campo di ricerca, riprova. ")
        os.system("sleep 1 && clear")
        ComePossoCostruire()

    for i in [item.lower() for item in titolo]:
        if progetto in i:
            progetti_trovati.append([i.lower()])                                                 # aggiunge ai progetti trovati i progetti corrispondenti al campo di ricerca

    if not progetti_trovati:
        title = (nome_programma + "\n \nNon ho trovato alcuna guida in merito, scegli l\'azione da intraprendere:")
        scelta_diy_non_trovato = ["Cerca online", "Crea una guida", "Torna al menu principale."]
        scelta_diy_non_trovato, index_diy_non_trovato = pick(scelta_diy_non_trovato, title, ">")
        if index_diy_non_trovato == 0:  # cerca come produrlo, su internet
            os.system("clear")
            dork = progetto.replace(" ", "+")
            os.system("xdg-open https://duckduckgo.com/?q=" + dork + "+produzione&t=ffab&atb=v258-4__&ia=web")
            os.system("xdg-open https://duckduckgo.com/?q=" + dork + "+diy&t=ffab&atb=v258-4__&ia=web")
            os.system("xdg-open https://duckduckgo.com/?q=" + dork + "+fai+da+te&t=ffab&atb=v258-4__&ia=web")
            os.system("xdg-open https://duckduckgo.com/?q=come+produrre+" + dork + "&t=ffab&atb=v258-4__&ia=web")
            os.system("xdg-open https://duckduckgo.com/?q=" + dork + "+diy&t=ffab&atb=v258-4__&ia=web")
            os.system("xdg-open https://it.wikipedia.org/wiki/" + dork)
            os.system("sleep 0.5 && clear")
            progetti_trovati.clear()
            start()
        elif index_diy_non_trovato == 1:
            # CreaGuida()
            progetti_trovati.clear()
            CreaGuida()
        else:
            print("Torno al menu principale...")
            os.system("sleep 0.5 && clear")
            progetti_trovati.clear()
            start()

    title = (nome_programma + "\n \nHo trovato i seguenti progetti:")                         # fa selezionare uno dei progetti trovati
    pdf_scelto = progetti_trovati
    if indietro not in pdf_scelto:
        pdf_scelto.append(indietro)
    pdf_scelto, index_scelta = pick(pdf_scelto, title, ">")
    if pdf_scelto == indietro:
        start()
    print ("Hai scelto ", pdf_scelto)
    scelta = (pdf_scelto.pop())
    os.system("clear")
    os.system("xdg-open " + (path[[item.lower() for item in titolo].index(scelta)]))
    os.system("sleep 0.5 && clear")
    progetti_trovati.clear()
    start()


def CercaManualmenteTraLeGuide():
    leggi_diy()
    options = titolo
    if indietro not in options:
        options.append(indietro)
    if len(options) < 2:
        title = (nome_programma + "\n \n Selezione manuale delle guide.\nEsplora le guide salvate sul tuo dispositivo:\n   Nessuna guida trovata!")
    else:
        title = (nome_programma + "\n \n Selezione manuale delle guide.\nEsplora le guide salvate sul tuo dispositivo:")
    options, index_diy_scelto = pick(options, title, ">")
    if options == indietro:
        start()
    else:
        os.system("xdg-open " + path[index_diy_scelto])  # apre il pdf scelto
        os.system("clear")
        banner()
        lista_verticale_elementi = '- '+('\n- '.join(map(str,lista_elementi[index_diy_scelto])))
        title = (nome_programma + "\n \n Lista dei materiali per costruire " + str(options) + ":" + "\n \n \n" + str(lista_verticale_elementi))
        options = ['>> Cerca uno degli elementi >>', indietro]
        options, index = pick(options, title, ">")
        if options == indietro:
            os.system("clear")
            os.system("sleep 0.5 && clear")
            CercaManualmenteTraLeGuide()
        elif options == ">> Cerca uno degli elementi >>":
            os.system("clear")
            os.system("sleep 0.5 && clear")
            ComePossoCostruire()


def CreaGuida():
    banner()
    print ("Inserisci il titolo del tuo progetto (sconsiglio di mettere un titolo gia' presente):")
    titolo_diy = input("  > ")                                                                                          # titolo del progetto

    os.system("clear")
    banner()
    nome_diy = titolo_diy.lower().replace(" ", "_") + ".diy"                                                            # nome del file diy
    print ("Il tuo progetto si chiama " + titolo_diy + " e lo troverai sotto " + working_directory_diy + nome_diy)
    print ("Specifica il path o trascina qui dentro il file del pdf della guida che vuoi creare:")
    path_pdf = input("  > ")                                                                                            # path del pdf

    os.system("clear")
    banner()
    print ("Inserisci ora la lista degli elementi che servono per costruire il tuo progetto, separandoli da una virgola e uno spazio (es.: nastro adesivo, chiodi, asse di legno):")
    elementi = input ("  > ")
    elementi_lista = elementi.split(", ")
    elementi_lista = "\n".join(elementi_lista)

    os.system("clear")
    title = (nome_programma + "\n \n Vado a creare il file " + nome_diy + " con titolo " + titolo_diy + ", il cui pdf si trova sotto il path " + path_pdf + "i cui elementi sono:\n" + elementi_lista)
    newdiy = open(working_directory_diy+nome_diy, "w")
    newdiy.write("[titolo]\n"+titolo_diy+"\n\n[elementi]\n"+elementi_lista+"\n\n[pdf]\n"+path_pdf+"\n")                 # scrive il file nuovo
    newdiy.close()
    print ("Hai appena creato la guida per "+titolo_diy+"!")
    os.system("sleep 1")
    start()


def CreaPDF():
    os.system("clear")
    banner()
    print("Inserendo il nome del tuo pdf andrai ad aprire LibreOffice Writer con il modello preimpostato in modo che ti sara\' piu\' facilitata la stesura della guida.\nTi ricordiamo dunque di salvare prima di uscire da Writer e di esportatre il pdf nella directory \n", working_directory_pdf, "\n\nInserisci il nome del pdf che vuoi scrivere:")
    pdf_da_creare = input("  > ")
    os.system("cp templates/pdf-template.odt templates/tmp/"+pdf_da_creare+".odt && libreoffice --writer templates/tmp/"+pdf_da_creare+".odt")
    print ("LibreOffice aperto, buona scrittura!")
    os.system("sleep 0.5")
    start()


def CaricaOnline():
    os.system("clear")
    title = (nome_programma+"\n \n***ATTENZIONE!***\nCaricando una guida sulla rete IPFS, verra\' mantenuto il tuo anonimato,\n ma il file condiviso in rete non potra\' piu\' essere eliminato,\n in quanto il file restera\' distribuito sui nodi anonimi della rete peer to peer.")
    options = ["Accetto", "Annulla"]
    options, index = pick(options, title, ">")
    if index == 0:
        os.system("clear")
        if not titolo:
            print (nome_programma,"\n\n\nNessuna guida trovata!")
            os.system("sleep 1 && clear")
            start()
        title = (nome_programma+"\n\nLa guida che andrai a caricare verra\' salvata nel file "+repository+"\nSeleziona la guida che vuoi caricare:")
        options = titolo
        options, index = pick(options, title, ">")
        options = options.lower()
        options = options.replace(" ","_")
        os.system("if [ -d "+repository+"tmp/"+options+" ]; then echo '"+repository+"tmp/"+options+" presente...'; else mkdir "+repository+"tmp/"+options+" && echo 'Creata directory "+repository+"tmp/"+options+"...'; fi")
        os.system("cp "+pathdiy[index]+" "+repository+"tmp/"+options)
        os.system("cp "+path[index]+" "+repository+"tmp/"+options)
        print("Sto caricando il pacchetto da condividere.\nAttenedere...")
        os.system("tar -cjf "+repository+"share/"+options+".tar.xz -C "+repository+"tmp/"+options+" .")
        os.system("ipfs add " + repository + "share/" + options + ".tar.xz | sed 's/added //g' | sed 's/"+options+".tar.xz//g' > hash")
        with open("hash", "r") as hash_aperto:                  # allora leggi il file
            hash = hash_aperto.readlines()                      # leggi l'hash del file caricato
        hash = str("".join(hash))
        repository_text=str(repository+"local.list")
        print (repository_text)
        with open(repository_text, "r+") as repo:  # apre il file di repository
            for line in repo:
                if options in line:
                    title = (nome_programma + "\n\nIl file risulta presente e gia\' caricato online sulla rete ipfs con link:\nhttps://ipfs.io/ipfs/" + hash + "\n\nCosa vuoi fare?")
                    options = ["Carica un\' altra guida", "Torna al menu\' principale"]
                    options, index = pick(options, title, ">")
                    if index == 0:
                        os.system("clear")
                        os.system("sleep 0.5")
                        CaricaOnline()
                    else:
                        os.system("clear")
                        os.system("sleep 0.5")
                        start()
            repo.write(str(options)+"\n"+"https://ipfs.io/ipfs/"+hash+"\n\n")                         # scrive i repository in local.list
            print (str(options)+" aggiunto ai repository locali come: https://ipfs.io/ipfs/"+hash)
            print ("\nPrima che il link risulti funzionante aspetta che qualche minuto, il tempo necessario che il file si distribuisca su abbastanza nodi della rete p2p.\n\n")
        print("Pulizia dei file temporanei in corso...")
        os.system("rm -f hash")
        os.system("rm -rf "+repository+"tmp/*")
        print("Pulizia completata!")
        os.system("sleep 2")
        title = (nome_programma+"\n\nCosa vuoi fare?")
        options = ["Carica un\'altra guida","Torna al menu\' principale"]
        options, index = pick(options, title, ">")
        if index == 0:
            os.system("clear")
            CaricaOnline()
        else:
            os.system("clear")
            start()
    else:
        os.system("clear")
        start()

def CercaOnline():
    curl_link=[]                                                # lista per l'output della curl in riferimento ai link dei repository
    curl_link_string = []
    repo = open("repository/remote.list", "r")                  # apre e legge file remote.list
    repo_links = repo.readlines()
    repo_links = ([s.strip('\n') for s in repo_links])
    if not repo_links:                                           # se la lista è vuota
        os.system("clear")
        print ("Errore sul file dei repository remoti, la lista sembra essere vuota.")
        print ("Torno al menu...")
        os.system("sleep 1")
        start()
    headers = {'x-api-key': '09ba90f6-dcd0-42c0-8c13-5baa6f2377d0'}     # header della richiesta pycurl
    for repo_link in repo_links:
        resp = requests.get(repo_link,headers=headers)                  # fa la curl sul link
        if resp.status_code != 200:                                     # se il link non funziona esce e comunica di verificare
            os.system("clear")
            print ("Errore sul repository ",repo_link)
            print ("Verificare che il file remote.list sia configurato correttamente.")
            print ("Esco...")
            os.system("sleep 1")
            os.system("cd repository && vim remote.list")
            exit()
        else:
            curl_link.append(resp.content)                              # crea la lista del contenuto dei wget in byte
    i=0
    lista_suddivisa_n = []                                              # lista delle singole righe del wget
    lista_titoli_wget = []                                              # lista dei singoli titoli ottenuti dal wget
    link_lista_suddivisi = []
    nomi_lista_suddivisi = []
    for n in curl_link:
        n = n.decode("utf-8")                                           # decodifica da byte in utf-8
        lista_suddivisa = n.split("\n")                                 # suddivide l'output ogni volta che va a capo creando una lista
        lista_suddivisa = list(filter(None, lista_suddivisa))           # toglie tutti i campi vuoti
        lista_suddivisa_n.append(lista_suddivisa)                       # aggiunge alla lista le singole parti creando una lista nidificata
        lista_titoli_wget.append(lista_suddivisa[0].strip('#'))         # crea la lista dei titoli dei repository entro cui entrare
    title = (nome_programma + "\n\nSeleziona un repository:")           # menu di selezione dei repository
    options = lista_titoli_wget
    if indietro not in options:
        options.append(indietro)
    options, index = pick(options, title, ">")
    if options == indietro:
        start()
    for link_lista_scelta in lista_suddivisa_n[index]:                  # per ogni output del wget scelto, nella lista
        if "https" in link_lista_scelta:                                # se è un link
            link_lista_suddivisi.append(link_lista_scelta)              # appendi il link alla lista dei link_lista_suddivisi
        else:
            nomi_lista_suddivisi.append(link_lista_scelta)              # appendi il nome della guida nella lista nomi_lista_suddivisi
    del nomi_lista_suddivisi[0]                                         # elimina il titolo dei repository dalla lista dei nomi delle guide
    title = (nome_programma + "\n\nSeleziona la guida che vuoi scaricare:") # menu per la selezione della guida
    options = nomi_lista_suddivisi
    options, index = pick(options, title, ">")
    print (nome_programma)
    print ("\nSto scaricando il file, ci potrei mettere qualche minuto a scaricarlo.\n Il file si trova attualmente su qualche nodo della rete ipfs, pertanto sto trovando i nodi che dispongono di quel file in modo da poterlo fartelo avere in totale anonimato e senza connessioni centralizzate.")
    print ("In caso di timeout ti invitiamo a contattare il distributore del repository e a segnalargli il problema. \nLa rete ipfs non si basa su servizi a pagamento o su societa' esterne, pertanto e' necessario contribuire alla risoluzione di un problema qualora si presenti.\n\n")
    os.system('rm -f tmp/*; cd tmp; wget --no-verbose -c '+link_lista_suddivisi[index]+' -T 10 -O guida.bz2; bzip2 -d guida.bz2; tar -xvf guida; cd -; mv -f tmp/*.diy '+working_directory_diy+'; mv -f tmp/* '+working_directory_pdf)       # scaricamento della guida, posizionamento in tmp e successivamente nelle rispettive cartelle
    print ("Nuova guida scaricata ed aggiunta!")
    os.system("sleep 0.5 && clear")
    title = (nome_programma + "\n\nCosa vuoi fare?")                    # torna al  menu o scarica un'altra guida
    options = ['Scarica un\'altra guida','Torna al menu']
    options, index = pick(options, title, ">")
    if index == 0:
        os.system("clear")
        CercaOnline()
    else:
        os.system("clear")
        start()

def cancellaGuide():
    options = titolo
    if not options:
        os.system("clear")
        print("Non hai ancora nessuna guida!\nTorno al menu principale.")
        os.system("sleep 1 && clear")
        start()
    else:
        title = (nome_programma + "\n \n Cancella una guida.\nSeleziona la guida che vuoi eliminare:")
    if indietro not in options:
        options.append(indietro)
    options, index_diy_scelto = pick(options, title, ">")
    if options == indietro:
        start()
    else:
        banner()
        title = (nome_programma + "\n \n Vuoi veramente eliminare la guida " + str(options) + "?")
        guida = str(options)
        options = ['Si\', elimina la guida pdf e il file di configurazione diy.', 'No, torna indietro.', indietro]
        options, index = pick(options, title, ">")
        if options == 'Si\', elimina la guida pdf e il file di configurazione diy.':
            os.system("clear")
            try:
                print("Sto rimuovendo il file pdf...")
                os.remove(path[index_diy_scelto])
                print("Sto eliminando il file diy...")
                os.remove(pathdiy[index_diy_scelto])
            except Exception as Ex:
                print("Impossibile eliminare il file diy e il file pdf per", str(guida))
                print("ERRORE:", Ex)
                exit()
            print("Guida", str(guida), "eliminata!")
            leggi_diy()
            os.system("sleep 1 && clear")
            start()
        elif options == 'No, torna indietro.':
            os.system("clear")
            os.system("sleep 0.5 && clear")
            cancellaGuide()
        elif options == indietro:
            os.system("clear")
            os.system("sleep 0.5 && clear")
            cancellaGuide()

#Inizializza il programma
animation()                     # animazione
leggi_diy()                     # inizializza le variabili per i diy e i pdf
start()                         # inizia il programma
